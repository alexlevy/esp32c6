use core::fmt::Debug;
use esp32c6_hal::rmt::{PulseCode, TxChannel, TxChannelConfig};
use fugit::Rate;

/// RGB LED peripheral present on certain ESP32 boards (e.g. on GPIO8 on esp32-c6)
pub struct NeoPixel<TX: TxChannel<CHANNEL> + Debug, const CHANNEL: u8> {
    freq: u16,
    channel: TX,
    t0: PulseCode,
    t1: PulseCode,
}

#[derive(Clone, Copy)]
pub struct RGB(pub u8, pub u8, pub u8);

mod internal {
    use crate::drivers::neopixel::RGB;
    use esp32c6_hal::rmt::PulseCode;

    pub struct NeopixelRGB<'a> {
        rgb: RGB,
        freq: u16,
        t0: &'a PulseCode,
        t1: &'a PulseCode,
    }

    impl<'a> NeopixelRGB<'a> {
        pub fn new(rgb: RGB, freq: u16, t0: &'a PulseCode, t1: &'a PulseCode) -> Self {
            Self { rgb, freq, t0, t1 }
        }

        fn pulse_from_bit(&self, color: u8, bit: usize) -> PulseCode {
            if (color >> bit) & 0x1 == 0 {
                PulseCode::from(*self.t0)
            } else {
                PulseCode::from(*self.t1)
            }
        }

        fn reset_pulse(&self) -> PulseCode {
            let _25us = (self.freq * 50) >> 1;

            PulseCode {
                level1: false,
                length1: _25us,
                level2: false,
                length2: _25us,
            }
        }

        fn set_pulses(&self, pulses: &mut [PulseCode; 26], color: u8, offset: usize) {
            for bit in 0..8 {
                pulses[bit + offset] = self.pulse_from_bit(color, 7 - bit);
            }
        }
    }

    impl<'a> Into<[PulseCode; 26]> for NeopixelRGB<'a> {
        fn into(self) -> [PulseCode; 26] {
            let mut pulses: [PulseCode; 26] = [PulseCode::default(); 26];

            // Reset
            pulses[0] = self.reset_pulse();

            // Transmit G, R, B in this order
            self.set_pulses(&mut pulses, self.rgb.1, 1);
            self.set_pulses(&mut pulses, self.rgb.0, 9);
            self.set_pulses(&mut pulses, self.rgb.2, 17);

            pulses
        }
    }
}

impl<TX: TxChannel<CHANNEL> + Debug, const CHANNEL: u8> NeoPixel<TX, CHANNEL> {
    pub fn new(freq: Rate<u32, 1, 1>, channel: TX, config: &TxChannelConfig) -> Self {
        let freq = freq.to_MHz() / (config.clk_divider as u32);

        let t0: u32 = (freq * 2 / 5) | 1 << 15 | ((freq * 17 / 20) << 16); // 0.4 us high followed by 0.85 us low
        let t1: u32 = (freq * 4 / 5) | 1 << 15 | ((freq * 9 / 20) << 16); // 0.8 us high followed by 0.45 us low

        Self {
            freq: (freq as u16),
            channel,
            t0: PulseCode::from(t0),
            t1: PulseCode::from(t1),
        }
    }

    /// Send pulses over [RMT](esp32c6_hal::rmt::Rmt) controller corresponding to the wanted `RGB` color
    ///
    /// # Arguments
    /// * `rgb` - An `RGB` tuple-struct instance encoding the wanted color and intensity
    ///
    /// Returns ownership of the [NeoPixel] instance.
    pub fn set_color(mut self, rgb: RGB) -> Self {
        let rgb = internal::NeopixelRGB::new(rgb, self.freq, &self.t0, &self.t1);
        let data: [PulseCode; 26] = rgb.into();
        let transaction = self.channel.transmit(&data);
        self.channel = transaction.wait().unwrap();
        self
    }
}
