#![no_std]
#![no_main]

use esp32c6_hal::entry;
use esp_backtrace as _;
use esp_println::logger::init_logger;

#[entry]
fn main() -> ! {
    init_logger(log::LevelFilter::Debug);
    loop {}
}
