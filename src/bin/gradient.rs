#![no_std]
#![no_main]

use esp32c6_hal::{
    clock::ClockControl,
    peripherals::Peripherals,
    prelude::*,
    rmt::{TxChannelConfig, TxChannelCreator},
    Delay, Rmt, IO,
};
use esp_backtrace as _;
use esp_println::logger::init_logger;

use esp32c6::drivers::neopixel::{NeoPixel, RGB};

#[entry]
fn main() -> ! {
    init_logger(log::LevelFilter::Debug);

    let peripherals = Peripherals::take();

    let system = peripherals.SYSTEM.split();

    let clocks = ClockControl::boot_defaults(system.clock_control).freeze();

    let mut delay = Delay::new(&clocks);

    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    let pin8 = io.pins.gpio8;

    let rmt = Rmt::new(peripherals.RMT, 80u32.MHz(), &clocks).unwrap();

    let chan_cfg = TxChannelConfig {
        clk_divider: 2,
        idle_output: true,
        idle_output_level: false,
        ..TxChannelConfig::default()
    };

    let chan = rmt.channel0.configure(pin8, chan_cfg).unwrap();

    let mut neo = NeoPixel::new(80u32.MHz(), chan, &chan_cfg);

    let mut n: i8 = 0;
    let mut inc: i8 = 1;
    let lumi: i8 = 64;

    loop {
        n = (n + inc) % lumi;
        let inv_color = lumi as u8 - 1 - n as u8;
        neo = neo.set_color(RGB(inv_color, n as u8, inv_color >> 2));
        if n == 0 || n == lumi - 1 {
            inc = -inc;
        }
        delay.delay_ms(34u32); // Roughly 30Hz refresh rate
    }
}
